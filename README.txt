DrupalFever API is a set of tools designed to help the Drupal developer to create new modules and themes. 
Following is a list of the available Features:

 1)  df_kill
     Description: This function can only be used if you are using the Boost module. The Boost module create 
     cached versions of your webpages and can reatly speedup your website. However, in certain occasions 
     you may want delete the cached version of a single page on your website. That's where the df_kill 
     function can prove its usefulness. You can call it to delete a single cached page from the Boost
     cache folder.
  
 2)  df_model
     Description: This function can only be used if you are using the Ubercart module. Ubercart is the 
     premiere shopping cart for Drupal 6. Sometimes you want to access a product's page but we only 
     have the product's model number. With this little function at hand you can provide the product's 
     model number to your website and you will be delivered to the node that corresponds to the referred 
     product.
     
 3)  df_boost_kill
     Description: This function can only be used if you are using the Boost module. This function is very 
     similar to the df_kill function. The only difference is that df_boost_kill can only delete cached 
     versions of nodes and it can also be called from code. It will kill the cached version of the node 
     page created by the Boost module. At the end of the proceadure it will forward the user to the node 
     location, creating, therefore, a new cached version.
     
 4)  df_display_table
     Description: This function will get a database table and generate the html to display that table.
     It can be quite adaptable. It will returns an html string with a pretty display of the database table.

 5)  df_gender
     Description: This function will determine the gender of a name. It currently has 8,058 female names and  
     9,477 male names in it's database. There is also an interface that would allow you to add new names or 
     remove existing names. It will return "Mr. " if the given first name is a male name or "Ms. " if it is 
     a female name. Will return an empty string if the name is not in our growing list of names.
     
 6)  df_move_file
     Description: This function will:
     * Create a folder if not existent
     * Move the file into the new location
     * Update the file record on the database
     
 7)  df_nid_img
     Description: This is a usefull tool if you have the Image module enabled and you have a CCK image field 
     that is associated with a node. On Ubercart, for example, you can associate a product's node with an 
     image. This function wil help you find out that product's image file path. It will return a Drupal path 
     of the image associated with that product node.

 8)  df_nid_model
     Description: This function can only be used if you are using the Ubercart module. It will grab the model 
     number from the database for a given node id.
     
 9)  df_once
     Description: This function can prove to be very useful during development and debugging. In some instances
     while tracking a loop, we want to fire the dsm command from the Devel module but, you end up firing it 
     each time. It the dsm returned array is too big, you may end up crashing the website. With this little 
     timer you may runn your dsm test only once and, then weit for the next 30 seconds. This function will 
     return TRUE only once until the established delay time is reached. If, optionally, you call this function 
     like this "df_once(10)" you will get FALSE for the next 30 seconds unless you call this function 10 times.

10)  df_ordinal
     Description: This is a very simple one. This function will add a 2 letter extencion for the ordinal 
     equivalent of the given number.
     Ex.:
     1 => 1st
     3 => 3rd
     5 => 5th
     
11)  df_query
     Description: This is another tool to help in debugging SQL statements. You must have Devel enabled to use 
     this one. This funciton will simulate a db_query in the active database. It will not execute the query. It 
     will just return a SQL query string with the arguments replaced. It is pretty useful to check a db_query.

12)  df_rename
     Description: Copies a file to a new location.
     This is a powerful function that in many ways performs like an advanced version of copy().
     * Checks if $source and $dest are valid and readable/writable.
		 * Performs a file copy if $source is not equal to $dest.
		 * If file already exists in $dest either the call will error out, replace the
		   file or rename the file based on the $replace parameter.

13)  df_rss
     Description: This function will convert any HTML string into a RSS friendly XML string.
 
14)  df_select
     Description: This function will generate a SELECT query based on the information provided. This is another 
     big one. The function will return the result of the query as an associative array. However, a string will 
     be returned if the $fields parameter has only one field and the resulting query has only one value.

15)  dfchk_email
     Description: This is a simple function that will check the syntax of an email address.
     
16) dfdsm
    Description: This function depends on the Devel module. It can be used just like a db_query function. 
    However, instead of executing a query, it will just print the query out with a dsm.
