<?php

/**
 * @desc This image will load an image from file. Only PNG, JPG and GIF are supported.
 * @param $img_url (String) Required -> A relative or absolute path to an image. Ex.: $imgname = "sites/default/files/test.jpg";
 * @return Returns an image resource identifier on success or FALSE if there was an error.
 */
function df_img_load ($img_url) {

  $path_parts = pathinfo($img_url);
  $extension = strtolower($path_parts['extension']);
  $supported_extension = TRUE;

  switch (strtolower($extension)) {
    
    case ('jpg'):

      // Attempt to open JPEG
      $im = @imagecreatefromjpeg($img_url);
      break;

    case ('jpeg'):

      // Attempt to open JPEG
      $im = @imagecreatefromjpeg($img_url);
      break;

    case ('png'):

      // Attempt to open PNG
      $im = @imagecreatefrompng($img_url);
      
      //Set the flag to save full alpha channel information.
      imagesavealpha($im, true);
      
      break;

    case ('gif'):

      // Attempt to open GIF
      $im = @imagecreatefromgif($img_url);
      break;

    default :

      $supported_extension = FALSE;
  }

  // See if it is a supported extension.
  if ($supported_extension) {

    // See if it failed
    if(!$im) {

      drupal_set_message('There was an error loading the image ' . $img_url, 'error');
    }
  }
  else {

    drupal_set_message('The image submitted "' . $img_url . '" has an unsupported format! Only PNG, JPG and GIF are allowed.', 'error');
  }

  return $im;
}

/**
 * @desc This function will save an image resource into a given file path. The function supports JPG, GIF and PNG image formats.
 * @param $dst_im (Image Resource) Required -> An image resource. Ex. $src_im = df_img_load('sites/default/files/source.png');
 * @param $dst_url (String) Required -> The relative path of the destination file. Ex. $dst_url = 'sites/default/files/destination.png';
 * @return Returns TRUE on success or FALSE on failure.
 */
function df_img_save ($dst_im, $dst_url) {

  //Get the destination image type.
  $path_parts = pathinfo($dst_url);
  $type = strtolower($path_parts['extension']);

  //Use the appropriate function depending on the image type to save the new image.
  switch($type){

    case ('gif'):

      imagegif($dst_im, $dst_url);
      break;
    case ('jpg'):

      imagejpeg($dst_im, $dst_url);
      break;
    case ('png'):

      imagepng($dst_im, $dst_url);
      break;
    default :

      drupal_set_message('The destination image has an unsupported type(' . $type . ')!');
    return FALSE;
  }

  return TRUE;
}

/**
 * @desc This function will figure out the measurements in pixels of any string.
 * @param $string (String) Required -> The string that needs to be measured. Ex. $string = "Test String";
 * @param $font_path (String) Required -> The full or relative path of the TrueType Font used on the string. Ex. $font_path = "sites/default/files/arial.ttf";
 * @param $font_size (Float) Optional -> The font size. The default value is 10. Ex. $font_size = 12;
 * @param $font_angle (Float) Optional -> The angle in which the text will be tilted. The default value is 0. Ex. $font_angle = 45.2;
 * @return It will return an associative array with the "width" & "height" values in pixels. Ex. return array('width' => 44, 'height' => 70);
 */
function df_str_metrics ($string, $font_path, $font_size = 10, $font_angle = 0) {

  //Get exact dimensions of text string
  $box = @imageTTFBbox($font_size, $font_angle, $font_path, $string);

  //Get width of text from dimensions
  $textwidth = abs($box[4] - $box[0]);

  //Get height of text from dimensions
  $textheight = abs($box[5] - $box[1]);

  return array('width' => $textwidth, 'height' => $textheight);
}

/**
 * @desc Converts Hexadecimal color (HTML Color) to RGB color.
 * @param $color (String) Required -> A string with the Hexadecimal color (HTML Color). Ex. $color = '#7FFFD4';
 * @return Returns an associative array with the RGB representation of the given Hexadecimal color. Ex. return array('R' => 127, 'G' => 255, 'B' => 212);
 */
function df_html2rgb($color) {

  $h = $color;

  if ($h[0] == '#') {

    $h = substr($h, 1);
  }

  if (strlen($h) == 6) {

    $r = $h[0] . $h[1];
    $g = $h[2] . $h[3];
    $b = $h[4] . $h[5];
  }
  elseif (strlen($h) == 3) {

    $r = $h[0] . $h[0];
    $g = $h[1] . $h[1];
    $b = $h[2] . $h[2];
  }
  else {

    return false;
  }

  $r = hexdec($r);
  $g = hexdec($g);
  $b = hexdec($b);

  return array('R' => $r, 'G' => $g, 'B' => $b);
}

/**
 * @desc  This function will write the given text into the image provided. You can submit more than one line of text
 *        at a time, each line with its own settings. Each line of text can be Centralized or adjusted to the Right margin.
 *        Each line of text can also be aligned vertically to the Middle or Bottom of the given image. You can submint an
 *        image in the PNG, JPG or GIF formats and save the new image into a different format such as PNG, JPG or GIF.
 *
 * @param $img_path (String) Required -> The full or relative path of the original image. Ex. $img_path = 'http://www.example.com/sites/default/files/diamond.jpg';
 * @param $new_path (String) Required -> The relative path of the destination image. It will not work if you provide the absolute path. Ex. $new_path = 'sites/default/files/diamond-specs.png';
 * @param $text_lines (Array) Required -> A Multidimensional Array that will provide each line of text with its own properties. Following is a list of all the expected properties per line.
 * Ex.:
 *
 * $text_lines = array (
 *   array (                                              //(Array)   Required => You must provide one array per line of text. At list one array with the fields "text" is required.
 *    'text'        => 'Test text...',                    //(String)  Required -> The line of text to insert into the provided image.
 *    'font_url'    => 'sites/default/files/arial.ttf',   //(String)  Optional -> The path of the font to use when generating the text. If not set will use the $default_font property.
 *    'font_size'   => '12',                              //(Float)   Optional -> The size of the font. The defaut value is 10. (Depending on how your server is configured it will be in points or pixels)
 *    'font_color'  => '#7FFFD4',                         //(Hex)     Optional -> The color in Hexadecimal (HTML Format).  If not provided, will use the $default_color property.
 *    'angle'       => '0',                               //(Float)   Optional -> The angle in which the line of text will be tilted. The defaut value is 0
 *    'x'           => '20',                              //(Integer) Optional -> The horizontal coordenate in which the line of text will be positioned. The defaut value is 0.
 *    'y'           => '20',                              //(Integer) Optional -> The vertical coordenate in which the line of text will be positioned. The defaut value is 0.
 *    'x_align'     => 'center',                          //(String)  Optional -> The horizontal alignment. You can set this as "center" or "right". The default horizontal alignment is "left".
 *    'y_align'     => 'middle',                          //(String)  Optional -> The vertical alignment. You can set this as "middle" or "bottom". The default vertical alignment is "top".
 *   )
 * );
 * @param $default_color (Hex) Optional -> The color in Hexadecimal (HTML Format). The defaut value is "#000000" (black).
 * @param $default_font (String) Optional -> The path of the font to use when generating the text. The default font is "Arial TrueType". Ex. $default_font = 'sites/default/files/arial.ttf';
 *
 * @return Will return TRUE or FALSE depending on whether the proceadure was successfull or not.
 */
function df_txt2img ($img_path, $new_path, $text_lines = array(array()), $default_color = '#000000', $default_font = '') {

  $default_font = ($default_font == '')? dfroot('drupalfever_api', TRUE) . 'font/arial.ttf': $default_font;

  $path_parts = pathinfo($img_path);
  $extension = strtolower($path_parts['extension']);
  $allowed = array('jpg', 'jpeg', 'png', 'gif');

  if (!in_array($extension, $allowed)) {

    drupal_set_message('The image provided has an extension "' . $extension . '" that is not supported. The only allowed image formats are JPG, PNG and GIF.', 'error');
    return FALSE;
  }

  //Get the height & width of the given image...
  list($img_width, $img_height) = getimagesize($img_path);

  //Load the given image...
  $img = df_img_load($img_path);

  $i = 1;

  //Loop through all the lines of text provided...
  foreach ($text_lines as $txt) {

    //If any of the lines of text have lass than the minimum number of the fields allowed, abort the whole proceadure...
    if ($txt['text'] != '') {

      //Set default values...
      $text = $txt['text'];
      $font_url = ($txt['font_url'] == '')? $default_font: $txt['font_size'];
      $font_size = ($txt['font_size'] == '')? 10: $txt['font_size'];
      $font_color = ($txt['font_color'] == '')? $default_color: $txt['font_color'];
      $font_RGB = df_html2rgb($font_color);
      $x = ($txt['x'] == '')? 0: $txt['x'];
      $y = ($txt['y'] == '')? 0: $txt['y'];
      $angle = ($txt['angle'] == '')? 0: $txt['angle'];
      $font_colr = df_get_color ($img, $font_RGB['R'], $font_RGB['G'], $font_RGB['B']);
      $x_align = $txt['x_align'];
      $y_align = $txt['y_align'];

      $text_metrics = df_str_metrics($text, $font_url, $font_size, $angle);
      $txt_width = $text_metrics['width'];
      $txt_height = $text_metrics['height'];

      $txt_x = $x;
      $txt_y = $y + $txt_height;

      //If Horizontal Alignemnt is set...
      if ($x_align != '') {

        switch ($x_align) {

          //If Aligning Right...
          case ('right'):

            //If no Horizontal Position was set, ajust to the center of the image.
            if ($txt['x'] == '') {

              $x = $img_width;
            }

            $txt_x = $x - $txt_width;
            break;

          case ('center'):

            //If no Horizontal Position was set, ajust to the center of the image.
            if ($txt['x'] == '') {

              $x = $img_width / 2;
            }

            $txt_x = $x - ($txt_width / 2);
            break;
        }
      }

      //If Vertical Alignemnt is set...
      if ($y_align != '') {

        switch ($y_align) {

          //If Vertical Alignment was set to bottom...
          case ('bottom'):

            //If no Vertical Position was set, ajust to the center of the image.
            if ($txt['y'] == '') {

              $y = $img_height;
            }

            $txt_y = $y;
            break;

          case ('middle'):

            //If no Vertical Position was set, ajust to the center of the image.
            if ($txt['y'] == '') {

              $y = $img_height / 2;
            }

            $txt_y = $y + ($txt_height / 2);
            break;
        }
      }

      if (($txt['angle'] == '90')
          && ($txt['x'] == '')) {

        $txt_x = $txt_x + $txt_width;
      }

      $txt_x = (round($txt_x));
      $txt_y = (round($txt_y));

      imagettftext($img, $font_size, $angle, $txt_x, $txt_y, $font_colr, $font_url, $text);
    }
    else {

      drupal_set_message('The ' . df_ordinal($i) . ' line of text submitted does not have the "text" field on the array. You must provide at list the "text" property per line of text.', 'error');
      imagedestroy($img);
      return FALSE;
    }

    $i++;
  }

  $path_parts = pathinfo($new_path);
  $extension = $path_parts['extension'];

  switch (strtolower($extension)) {

    case ('jpg'):

      //Save in the JPG format
      imagejpeg($img, $new_path);
      break;

    case ('jpeg'):

      //Save in the JPG format
      imagejpeg($img, $new_path);
      break;

    case ('png'):

      //Save in the PNG format
      imagepng($img, $new_path);
      break;

    case ('gif'):

      //Save in the GIF format
      imagegif($img, $new_path);
      break;

    default :

      imagedestroy($img);
    return FALSE;
  }

  imagedestroy($img);


  return TRUE;
}

/**
 * @desc This function see if the given color can be assigned to the provided image. If the provided color cannot be assigned to the given image, the function will
 * suggest a color that closest match the provided color that can be used.
 * @param $img (Image Resource) Requited -> You can create an image resouirce from an image URL like the following... Ex. $img = @imagecreatefrompng('sites/default/files/test.png');
 * @param $R (Integer) Required -> An integer representing the RGB Red color. Ex. $R = 5;
 * @param $G (Integer) Required -> An integer representing the RGB Green color. Ex. $G = 200;
 * @param $B (Integer) Required -> An integer representing the RGB Blue color. Ex. $B = 50;
 * @return It will retunr a color identifier for the provided color or for the replacement color.
 */
function df_get_color($img, $R, $G, $B) {

  //Try to get the given color from the image color palette
  $color = imagecolorexact($img, $R, $G, $B);

  //If the requested color is not part of the image color palette...
  if($color == -1) {

    //If the image color palette was completely used up...
    if(imagecolorstotal($img) >= 255) {

      //Pick out a color from the image palette that is closest to the requested color.
      $color = imagecolorclosest($img, $R, $G, $B);
    }
    //If there is still room in the image color palette...
    else {

      //Assign a new color.
      $color = imagecolorallocate($img, $R, $G, $B);
    }
  }

  return $color;
}

/**
 * @desc This function will resize or crop an image. If you only provide the width, the height will be proportionally calculated. It will behave in the same way for the height as well.
 * @param $src (String) Required -> The full or relative image path. Ex. $src = 'sites/default/files/test.png';
 * @param $dst (String) Required -> Tis will be the path there the resized image should be saved.  Ex. $src = 'sites/default/files/test-resized.png';
 * @param $width (Integer) Optional* -> The width of the resized image in pixels. This parameter is optional only if you have provided the $height parameter. Ex. $width = 300;
 * @param $height (Integer) Optional* -> The height of the resized image in pixels. This parameter is optional only if you have provided the $width parameter. Ex. $height = 200;
 * @return Returns TRUE if successfull.
 */
function df_img_resize($src, $dst, $width = 0, $height = 0){

  //Try to get the dimensions of the original image.
  if(!list($w, $h) = getimagesize($src)) {

    //If dimensions could not be aquired...
    drupal_set_message("Unsupported picture type!");

    return FALSE;
  }

  //If width was provided...
  if ($width) {

    //If height was not provided...
    if (!$height) {

      //Calculate the proporsional height based on the provided width parameter.
      $height = abs($width * $h) / $w;
    }
  }
  //If width was not provided...
  else {

    //If height was provided...
    if ($height) {

      //Calculate the proporsional width based on the provided height parameter.
      $width = abs($height * $w) / $h;
    }
    //If neither height nor width were provided...
    else {

      drupal_set_message('You must provide either the $width or the $height property that you want to resize the image to.');

      return FALSE;
    }
  }

  //Get the source image type.
  $path_parts = pathinfo($src);
  $type = strtolower($path_parts['extension']);

  //Use the appropriate function depending on the image type to import the original image.
  switch($type){

    case ('bmp'):

      $img = imagecreatefromwbmp($src);
      break;
    case ('gif'):

      $img = imagecreatefromgif($src);
      break;
    case ('jpg'):

      $img = imagecreatefromjpeg($src);
      break;
    case ('png'):

      $img = imagecreatefrompng($src);
      break;
    default :

      drupal_set_message('The original image has an unsupported type(' . $type . ')!');
    return FALSE;
  }

  //Creat new image resource.
  $new = imagecreatetruecolor($width, $height);

  //Preserve transparency
  if (($type == "gif")
      || ($type == "png")){

    imagecolortransparent($new, imagecolorallocatealpha($new, 0, 0, 0, 127));
    imagealphablending($new, false);
    imagesavealpha($new, true);
  }

  //Copy the original image into the new image resource with the new size.
  imagecopyresampled($new, $img, 0, 0, 0, 0, $width, $height, $w, $h);

  //Get the destination image type.
  $path_parts = pathinfo($dst);
  $type = strtolower($path_parts['extension']);

  //Use the appropriate function depending on the image type to save the new image.
  switch($type){

    case ('bmp'):

      imagewbmp($new, $dst);
      break;
    case ('gif'):

      imagegif($new, $dst);
      break;
    case ('jpg'):

      imagejpeg($new, $dst);
      break;
    case ('png'):

      imagepng($new, $dst);
      break;
    default :

      drupal_set_message('The destination image has an unsupported type(' . $type . ')!');
    return FALSE;
  }

  return TRUE;
}

/**
 * @desc This function will merge two images just like PHP's core imagecopymerge function. This function, however, will 
 * automatically load the images from an URL instead of requiring image resources. It will also preserver the images alpha 
 * channels. In addition, the resulting image resource will be saved in a file instead of returning an image resource. This 
 * function supports JPG, GIF and PNG file formats.
 * 
 * @param $bg_url (String) Required -> The relative path of the image used as background. Ex. $bg_url = 'sites/default/files/background.png';
 * @param $top_url (String) Required -> The relative or absolute path of the top layer image, which will be merged with the background image. Ex. $scd_url = 'sites/default/files/top-layer.png';
 * @param $dst_url (String) Required -> The desired relative path of the final image file. Ex. $dst_url = 'sites/default/files/destination.png';
 * @param $areas (Array) Optional -> This parameter must be a multidimensional array. The internal arrays are associative arrays. 
 * There has to be one associative array per desired area. If nothing is provided, the function will blend the two images on the 
 * left top most corner. Following is an example array.
 * 
 * Example:

$areas = array(
  array(
    'dst_x' => '5',   //(Integer) Optional -> The position of the destination image in the X axis. The default value is 0.
    'dst_y' => '7',   //(Integer) Optional -> The position of the destination image in the Y axis. The default value is 0.
    'src_x' => '5',   //(Integer) Optional -> The position of the source image in the X axis. The default value is 0.
    'src_y' => '15',  //(Integer) Optional -> The position of the source image in the Y axis. The default value is 0.
    'src_w' => '300', //(Integer) Optional -> The width of the source image. When no width is provided, will use the source file's full width minus the $src_x value.
    'src_h' => '30',  //(Integer) Optional -> The height of the source image. When no heigth is provided, will use the source file's full height minus the $src_y value.
    'pct' => '75',    //(Integer) Optional -> The two images will be merged according to the porcentage provided, which can range from 0 to 100. The default value is 100.
  ),
);

 * 
 * @return Returns TRUE on success or FALSE on failure.
 */
function multiarea_img_mrg($bg_url, $top_url, $dst_url, $areas){
  
  //Loade images from file into image resources
  $dst_im = df_img_load($bg_url);
  $src_im = df_img_load($top_url);
  
  foreach ($areas as $area) {
    
    //Set default values.
    $dst_x = (isset($area['dst_x']))? $area['dst_x']: 0;
    $dst_y = (isset($area['dst_y']))? $area['dst_y']: 0;
    $src_x = (isset($area['src_x']))? $area['src_x']: 0;
    $src_y = (isset($area['src_y']))? $area['src_y']: 0;
    $src_w = (isset($area['src_w']))? $area['src_w']: 0;
    $src_h = (isset($area['src_h']))? $area['src_h']: 0;
    $pct = (isset($area['pct']))? $area['pct']: 100;
    
    //Figure out the height and width of the source image if they are not provided.
    $src_w = ($src_w == 0)? imagesx($src_im): $src_w;
    $src_h = ($src_h == 0)? imagesy($src_im): $src_h;
    
    //Determine the destination height and width.
    $dst_w = imagesx($dst_im);
    $dst_h = imagesy($dst_im);
    
    //Bounds checking
    $src_x = max($src_x, 0);
    $src_y = max($src_y, 0);
    $dst_x = max($dst_x, 0);
    $dst_y = max($dst_y, 0);
    
    //Adjust source image width if it excede the destination image dimensions.
    if ($dst_x + $src_w > $dst_w) {
      
      $src_w = $dst_w - $dst_x;
    }
    
    //Adjust source image height if it excede the destination image dimensions.
    if ($dst_y + $src_h > $dst_h) {
      
      $src_h = $dst_h - $dst_y;
    }
    
    //Create an image resource with the given source dimensions.
    $cut = imagecreatetruecolor($src_w, $src_h);
  
    //Copying relevant section from background to the $cut image resource.
    imagecopy($cut, $dst_im, 0, 0, $dst_x, $dst_y, $src_w, $src_h);
  
    //Copying relevant section from watermark to the $cut resource.
    imagecopy($cut, $src_im, 0, 0, $src_x, $src_y, $src_w, $src_h);
    
    //Insert $cut resource to destination image.
    imagecopymerge($dst_im, $cut, $dst_x, $dst_y, 0, 0, $src_w, $src_h, $pct);
  }
  
  return df_img_save ($dst_im, $dst_url);
}