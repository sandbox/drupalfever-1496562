<?php

function df_kill () {
  
  $uri = str_replace('/dfkill', '', $_SERVER['REQUEST_URI']);
  $url = str_replace('/dfkill', '', $_SERVER['REDIRECT_URL']);
  
  if (strpos($uri, '?') !== FALSE) {
    
    $arguments = str_replace($url, '', $uri);
    $arguments = str_replace('?', '', $arguments);
  }
  else {
    
    $arguments = '';
  }
  
  $path = $url . '_' . $arguments . '.html';
  $path = 'cache/normal/' . $_SERVER['SERVER_NAME'] . $path;
  
  file_delete($path);
  file_delete($path . '.gz');
  drupal_goto(dfroot('/') . $uri);
}

function df_model ($model = '') {
  
  $df_sql = 'SELECT up.nid FROM {uc_products} up, {node} n WHERE up.model = "%s" AND n.nid = up.nid AND n.type = "product"';
  $df_query = db_query ($df_sql, $model);
  $df_array = db_fetch_array($df_query);
  
  $path = dfroot('http');
  
  if ($model == '1') {
    
    return 'Hello, Howard!';
  }
  elseif ($df_array['nid'] != '') {
    
    $path .= 'node/' . $df_array['nid'];
    drupal_goto($path);
  }
  elseif (trim($model) == '') {
    
    return 'Hello, Howard!';
  }
  else {
    
    drupal_set_message ('<br />&nbsp;&nbsp;&nbsp; The model number that you typed <b>"' . $model . '"</b> does not match any product on our inventory.<br /><br />', 'error');
    header('Refresh: 0; url=' . $_SERVER['PHP_SELF']);
  }
}

function df_diamond ($diamond = '') {

  $df_sql = 'SELECT up.nid FROM {uc_products} up, {node} n WHERE up.model = "%s" AND n.nid = up.nid AND n.type = "diamonds"';
  $df_query = db_query ($df_sql, $diamond);
  $df_array = db_fetch_array($df_query);

  $path = dfroot('http');

  if ($diamond == '1') {

    return 'Hello, Howard!';
  }
  elseif ($df_array['nid'] != '') {

    $path .= 'node/' . $df_array['nid'];
    drupal_goto($path);
  }
  elseif (trim($diamond) == '') {

    return 'Hello, Howard!';
  }
  else {

    drupal_set_message ('<br />&nbsp;&nbsp;&nbsp; The model number that you typed <b>"' . $diamond . '"</b> does not match any diamond on our inventory.<br /><br />', 'error');
    header('Refresh: 0; url=' . $_SERVER['PHP_SELF']);
  }
}