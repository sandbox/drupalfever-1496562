<?php

function drupalfever_api_settings () {
  
  $html .= '';
  
  return drupal_get_form('df_gender_form');
}

function df_gender_form () {
  
  $form = array();
  
  $form['df_gender_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('df_gender settings'),
    '#weight' => 5,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  
  $female = dfroot('drupalfever_api') . 'data/female_names.dat';
  $femaleNames = unserialize(file_get_contents($female));
  
  $form['df_gender_fieldset']['female_names'] = array(
    '#type' => 'select',
    '#title' => t('List of ' . count($femaleNames) . ' female names'),
    '#options' => (array('' => 'Select a name') + $femaleNames),
    '#description' => t('This is the current list of all the female names.'),
    '#prefix' => '<div style="width: 50%; float: left; height: 90px;">',
    '#suffix' => '</div>',
  );
  
  $form['df_gender_fieldset']['new_female_name'] = array(
    '#type' => 'textfield',
    '#title' => t('New female name'),
    '#size' => 35,
    '#maxlength' => 60,
    '#description' => t('Add a new name to the current list of female names by typing it here and clicking on the "Save Changes" button.'),
    '#prefix' => '<div style="width: 45%; float: right; height: 90px;">',
    '#suffix' => '</div><div title="Female first names only!" style="width: 5%; float: right; height: 90px;"><img style="margin-top: 16px;" src="' . dfroot('drupalfever_api') . 'images/female.png" alt="Female first names only!"></div>',
  );
  
  $male = dfroot('drupalfever_api') . 'data/male_names.dat';
  $maleNames = unserialize(file_get_contents($male));
  
  $form['df_gender_fieldset']['male_names'] = array(
      '#type' => 'select',
      '#title' => t('List of ' . count($maleNames) . ' male names'),
      '#options' => (array('' => 'Select a name') + $maleNames),
      '#description' => t('This is the current list of all the male names.'),
      '#prefix' => '<div style="width: 50%; float: left; height: 90px;">',
      '#suffix' => '</div>',
  );
  
  $form['df_gender_fieldset']['new_male_name'] = array(
      '#type' => 'textfield',
      '#title' => t('New male name'),
      '#size' => 35,
      '#maxlength' => 60,
      '#description' => t('Add a new name to the current list of male names by typing it here and clicking on the "Save Changes" button.'),
      '#prefix' => '<div style="width: 45%; float: right; height: 90px;">',
      '#suffix' => '</div><div title="Male first names only!" style="width: 5%; float: right; height: 90px;"><img style="margin-top: 16px;" src="' . dfroot('drupalfever_api') . 'images/male.png" alt="Male first names only!"></div>',
  );
  
  $form['df_gender_fieldset']['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save Changes'),
      '#prefix' => '<div style="width: 50%; float: left;"><br />',
      '#suffix' => '</div>',
  );
  
  $form['df_gender_fieldset']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete Selected'),
      '#prefix' => '<div style="width: 50%; float: right;"><br />',
      '#suffix' => '</div>',
  );
  
  return $form;
}

function df_gender_form_validate($form, &$form_state) {
  
}

function df_gender_form_submit($form, &$form_state) {
  
  if ($form_state['clicked_button']['#value'] == 'Save Changes') {
    
    $new_female_name = $form_state['values']['new_female_name'];
    
    if ($new_female_name != '') {
      
      $female = dfroot('drupalfever_api') . 'data/female_names.dat';
      $femaleNames = unserialize(file_get_contents($female));
      
      $new_female_name = strtoupper($new_female_name);
      
      if (!(in_array($new_female_name, $femaleNames))) {
        
        $femaleNames[] = $new_female_name;
        
        sort($femaleNames);
        
        $myFile = dfroot('drupalfever_api', TRUE) . 'data/female_names.dat';
        $fh = fopen($myFile, 'w') or die("can't open file");
        $stringData = serialize($femaleNames);
        fwrite($fh, $stringData);
        fclose($fh);
        
        drupal_set_message('The female name "' . ucfirst(strtolower($new_female_name)) . '" you entered has been added.');
      }
    }
    
    $new_male_name = $form_state['values']['new_male_name'];
    
    if ($new_male_name != '') {
      
      $male = dfroot('drupalfever_api') . 'data/male_names.dat';
      $maleNames = unserialize(file_get_contents($male));
    
      $new_male_name = strtoupper($new_male_name);
    
      if (!(in_array($new_male_name, $maleNames))) {
    
        $maleNames[] = $new_male_name;
    
        sort($maleNames);
    
        $myFile = dfroot('drupalfever_api', TRUE) . 'data/male_names.dat';
        $fh = fopen($myFile, 'w') or die("can't open file");
        $stringData = serialize($maleNames);
        fwrite($fh, $stringData);
        fclose($fh);
        
        drupal_set_message('The male name "' . ucfirst(strtolower($new_male_name)) . '" you entered has been added.');
      }
    }
  }
  else if ($form_state['clicked_button']['#value'] == 'Delete Selected') {
    
    $female_name = $form_state['values']['female_names'];
    
    if ($female_name != '') {
      
      $female = dfroot('drupalfever_api') . 'data/female_names.dat';
      $femaleNames = unserialize(file_get_contents($female));
      
      $deleted_name = ucfirst(strtolower($femaleNames[$female_name]));
      
      unset($femaleNames[$female_name]);
      sort($femaleNames);
      
      $myFile = dfroot('drupalfever_api', TRUE) . 'data/female_names.dat';
      $fh = fopen($myFile, 'w') or die("can't open file");
      $stringData = serialize($femaleNames);
      fwrite($fh, $stringData);
      fclose($fh);
      
      drupal_set_message('The female name "' . $deleted_name . '" you selected has been removed.');
    }
    
    $male_name = $form_state['values']['male_names'];
    
    if ($male_name != '') {
      
      $male = dfroot('drupalfever_api') . 'data/male_names.dat';
      $maleNames = unserialize(file_get_contents($male));
      
      $deleted_name = ucfirst(strtolower($maleNames[$male_name]));
      
      unset($maleNames[$male_name]);
      sort($maleNames);
      
      $myFile = dfroot('drupalfever_api', TRUE) . 'data/male_names.dat';
      $fh = fopen($myFile, 'w') or die("can't open file");
      $stringData = serialize($maleNames);
      fwrite($fh, $stringData);
      fclose($fh);
      
      drupal_set_message('The male name "' . $deleted_name . '" you selected has been removed.');
    }
  }
}